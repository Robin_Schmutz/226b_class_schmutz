﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace model
{
    [TestClass]
    public class TestPersonne
    {

        [TestMethod]
        public void createUser()
        {
            //given
            DateTime date = new DateTime(2002, 3, 3);
            string actualName = "Joseph Stoch";
            Personne perso = new Personne(actualName, date);

            string expectedMessage = "Joseph Stoch a 17 ans";
            string actualMessage = "";

            //when
            actualMessage = perso.ToString();
            //then
            Assert.AreEqual(expectedMessage, actualMessage);
        }
    }
    [TestClass]
    public class testVehicule
    {
        /// <summary>
        /// if the car is disponible, she can be used. If she's not, she can't be used.
        /// </summary>
        public void testConduire()
        {
            //given
            
            string actualName = "mazda";
            Vehicule voiture = new Vehicule(actualName)

            string expectedMessage = "vehicule disponible";
            string actualMessage = "";
            //when

            //then
            Assert.AreEqual(expectedMessage, actualMessage);
        }
    }
}
