﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace model
{
    public class Personne
    {
        private string name;
        private int age;
        /// <summary>
        /// this function is the constructor
        /// </summary>
        /// <param name="name"></param>
        /// <param name="date"></param>
        public Personne (string name, DateTime date)
        {
            this.name = name;
            DateTime today =  DateTime.Today;
            this.age = today.Year - date.Year;

        }
        /// <summary>
        /// this function change the function toString(). She has to put a message in who said the name of the people and is "age";
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            
            return name + " " + "a" + " " + age + " " + "ans";
        }
    }

}
